package com.br.jpw.JpwSaleService.demo.controller;

import com.br.jpw.JpwSaleService.demo.domain.Venda;
import com.br.jpw.JpwSaleService.demo.domain.VendaDTO;
import com.br.jpw.JpwSaleService.demo.service.VendaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/venda")
public class VendaController {

    @Autowired
    private VendaService vendaService;

    @PostMapping
    private ResponseEntity<VendaDTO> registrar(@RequestBody Venda venda){
        return ResponseEntity.status(HttpStatus.OK).body(vendaService.registrar(venda));
    }

    @GetMapping
    public ResponseEntity<List<VendaDTO>> listarTodos(){
        return ResponseEntity.status(HttpStatus.OK).body(vendaService.listarTodos());
    }

    @GetMapping("/{id}")
    public ResponseEntity<VendaDTO> listarPorId(@PathVariable Long id){
        return ResponseEntity.status(HttpStatus.OK).body(vendaService.listarPorId(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id){
        vendaService.deletarPorId(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

}
