package com.br.jpw.JpwSaleService.demo.step;

import com.br.jpw.JpwSaleService.demo.domain.Venda;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

public class Writer implements ItemWriter<Venda> {

    @Override
    public void write(List<? extends Venda> vendas) throws Exception {
        for (Venda venda: vendas) {
            System.out.println("Escrevendo fatura do produto" + venda.toString());
        }
    }
}
