package com.br.jpw.JpwSaleService.demo.domain;

import lombok.Data;

@Data
public class Cartao {

    private Long id;

    private String codigo;

    private String categoria;

    private Double limite;

}
