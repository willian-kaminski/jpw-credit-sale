package com.br.jpw.JpwSaleService.demo.step;

import com.br.jpw.JpwSaleService.demo.domain.Venda;
import org.springframework.batch.item.ItemProcessor;

public class Processor implements ItemProcessor<Venda, Venda> {

    @Override
    public Venda process(Venda venda) throws Exception {
        System.out.println("Processando....");
        return venda;
    }

}
