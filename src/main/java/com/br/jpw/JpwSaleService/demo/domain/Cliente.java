package com.br.jpw.JpwSaleService.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cliente {

    private Long id;

    private String nome;

    private String sexo;

    private String rg;

    private String cpf;

    private Double renda;

    private String email;

    private Date dataNascimento;

    private String telefone;

}
