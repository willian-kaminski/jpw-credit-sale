package com.br.jpw.JpwSaleService.demo.config;

import com.br.jpw.JpwSaleService.demo.domain.Venda;
import com.br.jpw.JpwSaleService.demo.step.Processor;
import com.br.jpw.JpwSaleService.demo.step.Reader;
import com.br.jpw.JpwSaleService.demo.step.Writer;
import com.br.jpw.JpwSaleService.demo.step.listener.JobCompletionListener;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class BatchConfig {

    public JobBuilderFactory jobBuilderFactory;
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job processJob() {
        return jobBuilderFactory.get("fatura-pocessor")
                .incrementer(new RunIdIncrementer()).listener(listener())
                .flow(orderStep()).end().build();
    }

    @Bean
    public Step orderStep() {
        return stepBuilderFactory.get("orderStep").<Venda, Venda> chunk(1)
                .reader(new Reader())
                .processor(new Processor())
                .writer(new Writer())
                .build();
    }

    @Bean
    public JobExecutionListener listener() {
        return new JobCompletionListener();
    }

}
