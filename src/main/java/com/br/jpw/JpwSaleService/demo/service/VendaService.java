package com.br.jpw.JpwSaleService.demo.service;

import com.br.jpw.JpwSaleService.demo.domain.Produto;
import com.br.jpw.JpwSaleService.demo.domain.Venda;
import com.br.jpw.JpwSaleService.demo.domain.VendaDTO;
import com.br.jpw.JpwSaleService.demo.repository.VendaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class VendaService {

    @Autowired
    private VendaRepository vendaRepository;

    private List<Produto> produtoList;

    public VendaDTO registrar(Venda venda){

        produtoList = Arrays.asList(
                new Produto(1L, "TV", "SAMSUNG", 3550D),
                new Produto(2L, "S9", "SAMSUNG", 2000D));

        for(Produto produto: produtoList){
            if(produto.getId() == venda.getIdProduto()){
                venda.setIdProduto(produto.getId());
            }
        }

        Venda vendaRealizada = vendaRepository.save(venda);
        return new VendaDTO(vendaRealizada);

    }

    public List<VendaDTO> listarTodos(){
        List<VendaDTO> vendaDTOList = VendaDTO.converterMotelToDTO(vendaRepository.findAll());
        return vendaDTOList;
    }

    public List<Venda> listar(){
        List<Venda> vendas = vendaRepository.findAll();
        return vendas;
    }

    public VendaDTO listarPorId(Long id){
        Venda vendaEncontrada = vendaRepository.findById(id).get();
        return new VendaDTO(vendaEncontrada);
    }

    public void deletarPorId(Long id){
        vendaRepository.deleteById(id);
    }

}
