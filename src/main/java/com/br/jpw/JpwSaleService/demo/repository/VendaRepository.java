package com.br.jpw.JpwSaleService.demo.repository;

import com.br.jpw.JpwSaleService.demo.domain.Venda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VendaRepository extends JpaRepository<Venda, Long> {
}
