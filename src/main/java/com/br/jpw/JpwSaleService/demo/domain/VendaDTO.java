package com.br.jpw.JpwSaleService.demo.domain;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class VendaDTO {

    private Long id;

    private Long clienteCPF;

    private Long idCartao;

    private Long idProduto;

    private LocalDateTime dataVenda;

    public VendaDTO() {
    }

    public VendaDTO(Venda venda) {
        this.id = venda.getId();
        this.clienteCPF = venda.getClienteCPF();
        this.idCartao = venda.getIdCartao();
        this.idProduto = venda.getIdProduto();
        this.dataVenda = venda.getDataVenda();
    }

    public static List<VendaDTO> converterMotelToDTO(List<Venda> paisList){
        return paisList.stream().map(VendaDTO::new).collect(Collectors.toList());
    }
}
