package com.br.jpw.JpwSaleService.demo.step;

import com.br.jpw.JpwSaleService.demo.domain.Venda;
import com.br.jpw.JpwSaleService.demo.service.VendaService;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import java.util.ArrayList;
import java.util.List;

public class Reader implements ItemReader<Venda> {

    private VendaService vendaService = new VendaService();
    private List<Venda> vendas;

    public Reader() {
        this.vendas = vendaService.listar();
    }

    @Override
    public Venda read() throws Exception, UnexpectedInputException,
            ParseException, NonTransientResourceException {

        for(int i = 0; i < vendas.size(); i++){
            return vendas.get(i);
        }

        return null;

    }

}
