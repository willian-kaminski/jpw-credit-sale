FROM openjdk:8-jdk-alpine

WORKDIR /app

COPY target/demo-0.0.1-SNAPSHOT.jar /app/jpw-service-sale.jar

ENTRYPOINT ["java", "-jar", "jpw-service-sale.jar"]